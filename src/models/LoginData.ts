import { CmsUser } from './CmsUser';

export interface LoginData {
  cmsUser: CmsUser;
  token: string;
}
