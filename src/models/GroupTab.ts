import { BrowserTab, IBrowserTab } from './Tab';

export interface GroupColor {
  text: string;
  background: string;
}

export interface IBrowserGroupTab {
  id: string;
  name: string;
  isEnable?: boolean;
  isEditing: boolean;
  isCollapsed: boolean;
  color?: GroupColor;
  tabs: {
    [id: string]: IBrowserTab;
  };
}

export class BrowserGroupTab implements IBrowserGroupTab {
  id: string;
  isEnable?: boolean;
  isEditing: boolean;
  isCollapsed: boolean;
  name: string;
  color?: GroupColor;
  tabs!: { [id: string]: IBrowserTab };

  static createNewTabInGroup(group: IBrowserGroupTab) {
    const tab = new BrowserTab();
    group.tabs[tab.id] = tab;
    tab.ownerId = group.id;
    return tab.id;
  }

  static getLastTabKey(group: IBrowserGroupTab) {
    const keys = Object.keys(group.tabs);
    return keys[keys.length - 1];
  }

  static isEmptyGroup(group: IBrowserGroupTab) {
    return !Object.keys(group.tabs).length;
  }

  constructor(name?: string) {
    this.id = `group-tab-${Date.now()}-${Math.floor(Math.random() * 1000)}`;
    this.name = name || '';
    this.tabs = {};
    this.isEnable = false;
    this.isEditing = false;
    this.isCollapsed = false;
    // BrowserGroupTab.createNewTabInGroup(this);
  }
}

export const GroupTabColors: GroupColor[] = [
  {
    background: 'seagreen',
    text: 'white'
  },
  {
    background: 'red',
    text: 'white'
  },
  {
    background: '#ff78cb',
    text: '#000'
  }
];
