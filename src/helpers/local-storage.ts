const DEVICE_KEY = 'ibe_cms-device';
const TOKEN_KEY = 'ibe_cms-token';

export default class LocalStorageHelper {
  public static get device() {
    return localStorage.getItem(DEVICE_KEY) || undefined;
  }
  public static set device(value) {
    localStorage.setItem(DEVICE_KEY, value || '');
  }

  public static get token() {
    return localStorage.getItem(TOKEN_KEY) || undefined;
  }
  public static set token(value) {
    localStorage.setItem(TOKEN_KEY, value || '');
  }
}
