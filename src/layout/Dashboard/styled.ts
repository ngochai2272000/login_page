import styled from 'styled-components';

type ExtendSvgProps = {
  radius?: number;
};

export const Root = styled.div`
  width: 100%;
  height: 100%;
`;

export const Svg = styled.svg`
  width: 100%;
  height: 100%;

  .round-track {
    fill: white;
    stroke: white;
    stroke-width: ${({ radius = 20 }: ExtendSvgProps) => 2 * radius};
    stroke-linejoin: round;
  }

  .left-container {
    width: 100%;
    height: 100%;
    background: var(--color-primary);
  }

  .main-container {
    width: 100%;
    height: 100%;
  }
`;
