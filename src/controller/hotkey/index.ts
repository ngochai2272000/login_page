import { ipcRenderer } from '../../cores/electron';
import { EventEmitter } from '../../utils/event-emitter/event-emitter';
import { StackEventEmitter } from '../../utils/event-emitter/stack-event-emitter';
import { IEventEmitter } from '../../utils/event-emitter/type';
import BrowserHotkey from './browser-hotkey';

const hotkeys = [
  'ctrl+`',
  'ctrl+s',
  'alt+left',
  'alt+right',
  'alt+1',
  'alt+2',
  'alt+3',
  'alt+4',
  'alt+5',
  'alt+6',
  'alt+7',
  'alt+8',
  'alt+9',
  'alt+0',
  'ctrl+m',
  'ctrl+space',
  'ctrl+left',
  'ctrl+right',
  'ctrl+shift+x',
  'ctrl+shift+r',
  'ctrl+shift+s',
  'ctrl+shift+f',
  'mediaplaypause',
  'mediastop',
  'medianexttrack',
  'mediaprevioustrack'
] as const;
type HotkeyType = typeof hotkeys[number];

type Listener = () => void;

class HotkeyController {
  _stackEmitter = new StackEventEmitter();
  _emitter = new EventEmitter();
  _sender?: IEventEmitter | Electron.IpcRenderer;

  constructor() {
    this._sender = ipcRenderer || new BrowserHotkey();
    this._sender.on('hotkey', (_, name: string) => {
      const event = name.toLowerCase();
      this._raiseHotkeyEvent(event);
    });
  }

  private _raiseHotkeyEvent = (event: string) => {
    if (!hotkeys.includes(event as HotkeyType)) {
      return;
    }
    if (this._stackEmitter.hasListener(event)) {
      this._stackEmitter.emit(event);
      return;
    }
    this._emitter.emit(event);
  };

  /**
   * Add listener to emitter. Just the last listener can be invoked
   * @param hotkey
   * @param listener
   * @returns
   */
  addListener(hotkey: HotkeyType, listener?: Listener) {
    if (listener) {
      this._stackEmitter.on(hotkey, listener);
    }
    return this;
  }

  removeListener(hotkey: HotkeyType, listener?: Listener) {
    if (listener) {
      this._stackEmitter.removeListener(hotkey, listener);
    }
  }

  /**
   * If not have any listener added by addListener function, all listeners added by this function can be invoked.
   * @param hotkey
   * @param listener
   * @returns
   */
  UNSAFE_addListener(hotkey: HotkeyType, listener: Listener) {
    this._stackEmitter.on(hotkey, listener);
    return this;
  }

  /**
   * Remove  listener added by UNSAFE_addListener function.
   * @param hotkey
   * @param listener
   */
  UNSAFE_removeListener(hotkey: HotkeyType, listener: Listener) {
    this._stackEmitter.removeListener(hotkey, listener);
  }
}

const hotkeyController = new HotkeyController();

export { hotkeyController as HotkeyController };
