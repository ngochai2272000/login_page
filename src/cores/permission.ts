import Enum from './enum';

export const AIAPermissions = Object.freeze({
  PLAY_AUDIO: 'PLAY_AUDIO' as 'PLAY_AUDIO'
});
export type AIAPermissions = Enum<typeof AIAPermissions>;

export interface IPermissionPayload {
  appId: string;
  permission: AIAPermissions;
  payload?: any;
}
