import { useState, useEffect } from 'react';
import { Button } from '../../components/Button';
import Input from '../../components/Input';
import { useHistory } from 'react-router';
import { withRouter } from 'react-router-dom';
import { Form } from './styled';
import styled from 'styled-components';
import { gql, useMutation } from '@apollo/client';

const Root = styled.div`
  display: flex;
  width: 100%;
  height: 100%;
  align-items: center;
  justify-content: center;
`;

function LoginPage() {
    const LOGIN = gql`
    mutation Login($username: String!, $password: String!) {
    login(username: $username, password: $password) {
      token
    }
  }
`;
    const token = localStorage.getItem("token")
    const history = useHistory();
    const [signIn, { data: signInData }] = useMutation(
        LOGIN
    )

    const handleFormSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        const username = e.currentTarget.username?.value;
        const password = e.currentTarget.password?.value;
        if (!username) {
            alert("Vui lòng nhập đủ thông tin username.");
        }
        if (!password) {
            alert("Vui lòng nhập đủ thông tin password.");
        }
        signIn({
            variables: {
                username,
                password,
            },
        }).then((result) => {
            if (result.data) {
                const data = result.data.login;
                localStorage.setItem('token', "Bearer " + data.token);
            }
            history.push('/');
        })
    };

    return (
        <Root>
            {token && history.push("/")}
            <Form onSubmit={handleFormSubmit}>
                <div className="form-row">
                    <Input
                        size="small"
                        name="username"
                        placeholder="Username"
                        label="Username"
                        variant="outlined"
                    />
                </div>
                <div className="form-row">
                    <Input
                        size="small"
                        type="password"
                        name="password"
                        placeholder="Password"
                        variant="outlined"
                        label="Password"
                    />
                </div>
                <div className="form-row">
                    <Button
                        type="submit"
                        color="primary"
                        variant="contained">
                        Login
                    </Button>
                </div>
            </Form>
        </Root>
    )
}


export default withRouter(LoginPage);