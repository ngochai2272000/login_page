import { Zoom } from '@material-ui/core';
import React from 'react';
import styled from 'styled-components';

type P = React.DetailedHTMLProps<
  React.HTMLAttributes<HTMLDivElement>,
  HTMLDivElement
> & {
  ref?:
    | ((instance: HTMLDivElement | null) => void)
    | React.RefObject<HTMLDivElement>
    | null
    | undefined;
};

const Root = styled.div`
  background: seagreen;

  :focus {
    outline: none;
  }
`;

export default function PageContainer({ children, ...rest }: P) {
  return (
    <Zoom in={true} mountOnEnter={true}>
      <Root tabIndex={1} {...rest}>
        {children}
      </Root>
    </Zoom>
  );
}
