import styled from 'styled-components';

export const Form = styled.form`
  max-width: 420px;
  width: 100%;
  padding: var(--space-4);
  box-sizing: border-box;
  border-radius: var(--space-4);
  background: var(--color-primary-hint);
  display: flex;
  flex-direction: column;
  .form-row {
    display: flex;
    flex-direction: column;
    margin: var(--space-2) 0;
  }
  .form-title {
    align-items: center;
    text-align: center;
    font-weight: 500;
    color: var(--color-primary);
    font-size: var(--font-size-h5);
  }
  .form-error {
    align-items: center;
    text-align: center;
    color: var(--color-danger);
    font-size: var(--font-size-small);
  }
`;