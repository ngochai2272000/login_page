import {
  Table as MuiTable,
  TableCell,
  TableHead,
  TableRow,
  withStyles,
} from '@material-ui/core';
import React from 'react';

export const Table = withStyles({
  root: {
    margin: '0 var(--space-4)',
    width: 'calc(100% - 2 * var(--space-4))',
    fontSize: 'var(--font-size)',
  },
})(MuiTable);

export const THead = withStyles({
  root: {
    color: 'var(--color-primary)',
    fontSize: '1.15em',
    fontWeight: 700,
    '& .MuiTableRow-head': {
      borderBottom: '2px solid var(--color-primary)',
    },
  },
})(TableHead);

export const TRow = withStyles({
  root: {
    cursor: ({ onClick }: { onClick?: (event: React.MouseEvent) => void }) =>
      onClick && 'pointer',
    '&:hover': {
      backgroundColor: 'var(--color-primary-hint)',
    },
  },
})(TableRow);

export const TCell = withStyles({
  root: {
    color: 'inherit',
    fontSize: 'inherit',
    padding: 'var(--space-4)',
    '&.first-cell': {
      paddingLeft: 'var(--space-2)',
    },

    '&.last-cell': {
      paddingRight: 'var(--space-2)',
    },
  },
  sizeSmall: {
    padding: 'var(--space-2)',
  },
})(TableCell);
