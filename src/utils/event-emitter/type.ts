export type EmitEventListener = (...args: any) => void;

export interface IEventEmitter {
  on(event: string, listener: EmitEventListener): IEventEmitter;
  emit(event: string, ...args: any[]): void;
  removeListener(event: string, listener: EmitEventListener): void;
  removeAllListeners(): void;
}
