export default class Stack<T> {
  _items: T[] = [];
  get length() {
    return this._items.length;
  }

  get first() {
    return this._items[0];
  }

  public push(item: T) {
    return this._items.unshift(item);
  }

  public pop() {
    return this._items.shift();
  }

  public remove(item: T) {
    this._items.filter((value) => value !== item);
  }
}
