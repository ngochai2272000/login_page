import { EventEmitter } from './event-emitter';

export type Listener = (...args: any) => void;

export class StackEventEmitter extends EventEmitter {
  emit(event: string, ...args: any[]) {
    if (this._events[event]?.length) {
      this._events[event][this._events[event].length - 1]?.apply(this, args);
    }
  }
}
