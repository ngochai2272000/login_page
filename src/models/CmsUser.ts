import { Role } from './Role';

export interface CmsUser {
  id: string;
  phone_number: string;
  email: string;
  roles: Role[];
}

export interface CmsUsers {
  data: CmsUser[];
  count: number;
}
