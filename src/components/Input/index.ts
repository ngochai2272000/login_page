import { TextField, withStyles } from '@material-ui/core';
import {
  __COLOR_PRIMARY_VAR,
  __COLOR_SECONDARY_VAR
} from '../../constance/style-var';

export default withStyles({
  root: {
    '& .MuiInputBase-root, label': {
      fontSize: 'inherit'
    },
    '& label.Mui-focused': {
      color: `var(${__COLOR_PRIMARY_VAR})`
    },
    '& .MuiInput-underline:after': {
      borderBottomColor: `var(${__COLOR_PRIMARY_VAR})`
    },
    '& .MuiOutlinedInput-root': {
      '& fieldset': {
        borderColor: `var(${__COLOR_PRIMARY_VAR})`
      },
      '&:hover fieldset': {
        borderColor: `var(${__COLOR_PRIMARY_VAR}-hover)`
      },
      '&.Mui-focused fieldset': {
        borderColor: `var(${__COLOR_SECONDARY_VAR})`
      }
    }
  }
})(TextField);
