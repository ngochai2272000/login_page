import { EventEmitter } from './event-emitter/event-emitter';

export default class Timer extends EventEmitter {
  private _code?: number;
  private _interval: number;
  public get interval() {
    return this._interval;
  }

  public set interval(value) {
    if (this._code) {
      throw Error('Cannot set interval while Timer is running');
    }
    this._interval = value;
  }

  get running() {
    return !!this._code;
  }

  constructor(interval = 333) {
    super();
    this._interval = interval;
  }

  start() {
    if (this._code) {
      return;
    }
    this._code = window.setInterval(() => this.emit('tick'), this._interval);
  }

  stop() {
    window.clearInterval(this._code);
    this._code = undefined;
  }
}
