Ibenefit CMS - ReactJS + Typescript
==============================================

- start:
```base
  yarn start
```
- build react:
```base
  yarn build
```
- build bundle (create index.html)
```base
  yarn dist
```