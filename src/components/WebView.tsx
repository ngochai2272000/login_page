import { WebviewTag } from 'electron';
import React, { WebViewHTMLAttributes } from 'react';
// @ts-ignore
import ElectronWebview from 'react-electron-web-view';
import styled from 'styled-components';

type P = WebViewHTMLAttributes<WebviewTag> & {
  webviewRef?: (webview: WebviewTag) => void;
  onNavigated?: (e: React.SyntheticEvent<WebviewTag>) => void;
  onPageTitleUpdated?: (e: React.SyntheticEvent<WebviewTag>) => void;
  onPageFaviconUpdated?: (e: { favicons: string[] }) => void;
  visible?: boolean;
};

const Root = styled.div`
  flex: 1;
  display: ${(p: { visible?: boolean }) => (p.visible ? 'initial' : 'none')};
  div,
  webview {
    width: 100%;
    height: 100%;
  }
`;

const WebView: React.FC<P> = ({
  className,
  id,
  visible,
  webviewRef,
  onLoad,
  onNavigated,
  ...rest
}) => {
  return (
    <Root visible={visible} className={className}>
      <ElectronWebview
        ref={webviewRef}
        onDidFinishLoad={onLoad}
        onDidNavigate={onNavigated}
        onDidNavigateInPage={onNavigated}
        className="webview"
        id={id}
        nodeIntegration={true}
        useragent="Chrome"
        {...rest}
      />
    </Root>
  );
};

export default WebView;
