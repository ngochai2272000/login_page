// import gql from '@apollo/client';
import React from 'react';
import {
  HashRouter as Router,
  // Redirect,
  Route,
  Switch
} from 'react-router-dom';
import styled from 'styled-components';
import { HomePage } from './Home';
import LoginPage from './Login/index';

const Root = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
`;

const MainTag = styled.main`
  flex: 1;
`;

const token = localStorage.getItem('token')

const Main: React.FC = () => {

  return (
    <Root>
      <MainTag>
        <Router>
          <Switch>
            <Route path="/login" component={LoginPage} />
            <Route path="/" component={HomePage} />
          </Switch>
        </Router>
      </MainTag>
    </Root>
  );
};

export default Main;
