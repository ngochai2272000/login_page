import React from 'react';
import { Root, Svg } from './styled';

type Props = {
  leftBlockWidth?: number;
  radius?: number;
  leftBlock?: React.ReactNode;
};

type State = {
  prevPoints?: string;
  points?: string;
  leftBlockWidth?: number;
  prevLeftBlockWidth?: number;
  width: number;
  height: number;
};

export default class DashboardLayout extends React.Component<Props> {
  static getDerivedStateFromProps(
    props: Readonly<Props>,
    state: Readonly<State>
  ) {
    const { leftBlockWidth = 120, radius = 32 } = props;
    const tilt = 30;
    const points = `${leftBlockWidth + tilt},${radius} ${
      state.width - radius
    },${radius} ${state.width - radius},${
      state.height - radius
    } ${leftBlockWidth},${state.height - radius}`;
    return {
      prevPoints: state.points,
      points,
      prevLeftBlockWidth: state.leftBlockWidth || 0,
      leftBlockWidth
    };
  }

  state = Object.freeze<State>({
    width: window.innerWidth,
    height: window.innerHeight
  });

  _svgRef = React.createRef<SVGSVGElement>();
  _animateRef = React.createRef<SVGGElement>();

  _requestFramed?: number;

  componentDidMount() {
    this._updateSize();
    window.addEventListener('resize', this._updateSize);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this._updateSize);
  }

  _updateSize = () => {
    if (this._requestFramed) {
      window.cancelAnimationFrame(this._requestFramed);
    }
    this._requestFramed = window.requestAnimationFrame(() => {
      const svgEl = this._svgRef.current;
      if (svgEl) {
        const bound = svgEl.getBoundingClientRect();
        this.setState({
          width: bound.width,
          height: bound.height
        });
      }
      this._requestFramed = undefined;
    });
  };
  componentDidUpdate() {
    if (this._animateRef.current) {
      Array.from(this._animateRef.current.children).forEach(
        (el: any) => typeof el.beginElement === 'function' && el.beginElement()
      );
    }
  }

  render() {
    const { radius = 32 } = this.props;
    const { leftBlockWidth } = this.state;
    const tilt = 30;
    const animationDur = '0.2s';

    return (
      <Root>
        <Svg ref={this._svgRef} radius={radius}>
          <foreignObject
            id="left-container-root"
            x={0}
            y={0}
            height={this.state.height}>
            <div className="left-container">{this.props.leftBlock}</div>
          </foreignObject>
          <polygon id="polygon" className="round-track" />
          <foreignObject
            id="main-container-root"
            y={radius}
            height={this.state.height - 2 * radius}>
            <div className="main-container">{this.props.children}</div>
          </foreignObject>
          <g ref={this._animateRef}>
            <animate
              xlinkHref="#main-container-root"
              attributeName="x"
              from={(this.state.prevLeftBlockWidth || 0) + tilt}
              to={(leftBlockWidth || 0) + tilt}
              fill="freeze"
              dur={animationDur}
            />
            <animate
              xlinkHref="#main-container-root"
              attributeName="width"
              from={
                this.state.width -
                ((this.state.prevLeftBlockWidth || 0) + tilt + radius)
              }
              to={this.state.width - ((leftBlockWidth || 0) + tilt + radius)}
              fill="freeze"
              dur={animationDur}
            />
            <animate
              xlinkHref="#left-container-root"
              attributeName="width"
              from={(this.state.prevLeftBlockWidth || 0) + tilt}
              to={(leftBlockWidth || 0) + tilt}
              fill="freeze"
              dur={animationDur}
            />
            <animate
              xlinkHref="#polygon"
              attributeName="points"
              from={this.state.prevPoints}
              to={this.state.points}
              fill="freeze"
              dur={animationDur}
            />
          </g>
        </Svg>
      </Root>
    );
  }
}
