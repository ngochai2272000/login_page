import React from 'react';
import { HotkeyController } from '../../controller/hotkey';
import Timer from '../../utils/timer';

export type MediaPlayerProps = {
  source?: string;
  onNext?: () => void;
  onPrevious?: () => void;
  onPosition?: (position: number) => void;
};

export type MediaPlayerState = {
  isPlaying: boolean;
  duration?: number;
  position?: number;
  volume: number;
  muted?: boolean;
};

const initState = Object.freeze<MediaPlayerState>({
  isPlaying: false,
  volume: 100
});

export default abstract class MediaPlayer<
  Props extends MediaPlayerProps,
  State extends MediaPlayerState
> extends React.Component<Props, State> {
  protected _isMouseDowned = false;
  protected _nextPosition?: number;
  protected _mediaRef = React.createRef<HTMLMediaElement>();
  protected _timer = new Timer();
  // @ts-ignore
  state: Readonly<State> = initState;

  _beforeUnmount?: () => void;
  _afterMount?: () => void;
  _close?: () => void;

  componentDidMount() {
    this._timer.on('tick', this._handleListen);
    //
    HotkeyController.addListener('ctrl+space', this._playOrPause);
    HotkeyController.addListener('mediaplaypause', this._playOrPause);
    HotkeyController.addListener('ctrl+left', this.props.onPrevious);
    HotkeyController.addListener('mediaprevioustrack', this.props.onPrevious);
    HotkeyController.addListener('ctrl+right', this.props.onNext);
    HotkeyController.addListener('medianexttrack', this.props.onNext);
    HotkeyController.addListener('ctrl+m', this._muteOrUnMute);
    HotkeyController.addListener('ctrl+shift+x', this._close);
    HotkeyController.addListener('mediastop', this._close);
    //
    this._afterMount?.();
  }

  componentWillUnmount() {
    this._beforeUnmount?.();
    //
    HotkeyController.removeListener('ctrl+space', this._playOrPause);
    HotkeyController.removeListener('mediaplaypause', this._playOrPause);
    HotkeyController.removeListener('ctrl+left', this.props.onPrevious);
    HotkeyController.removeListener(
      'mediaprevioustrack',
      this.props.onPrevious
    );
    HotkeyController.removeListener('ctrl+right', this.props.onNext);
    HotkeyController.removeListener('medianexttrack', this.props.onNext);
    HotkeyController.removeListener('ctrl+m', this._muteOrUnMute);
    HotkeyController.removeListener('ctrl+shift+x', this._close);
    HotkeyController.removeListener('mediastop', this._close);
    //
    this._timer.removeAllListeners();
  }

  protected _playOrPause = () => {
    const mediaEl = this._mediaRef.current;
    if (!mediaEl) {
      return;
    }
    if (this.state.isPlaying) {
      mediaEl.pause();
      return;
    }
    mediaEl.play();
  };

  protected _seek = (second: number = 0) => {
    const mediaEl = this._mediaRef.current;
    if (mediaEl) {
      mediaEl.currentTime = second;
      this.props.onPosition?.(second);
    }
  };

  protected _handleListen = () => {
    if (!this._isMouseDowned) {
      const position = this._mediaRef.current?.currentTime || 0;
      this.setState({ position }, () => {
        this.props.onPosition?.(position);
      });
    }
  };

  protected _handleMouseUp = () => {
    this._isMouseDowned = false;
    window.removeEventListener('mouseup', this._handleMouseUp);
    this._seek(this._nextPosition);
  };

  protected _handleMouseDown = () => {
    this._isMouseDowned = true;
    window.addEventListener('mouseup', this._handleMouseUp);
  };

  protected _handleSeekBarChange = (value: number) => {
    if (this._isMouseDowned) {
      this._nextPosition = value;
      this.setState({ position: value });
      return;
    }
  };

  protected _handlePlay: React.ReactEventHandler<HTMLMediaElement> = () => {
    this.setState({ isPlaying: true }, () => {
      this._timer.start();
    });
  };

  protected _handlePause: React.ReactEventHandler<HTMLMediaElement> = () => {
    this.setState({ isPlaying: false }, () => {
      this._timer.stop();
    });
  };

  protected _handleLoadedMetadata: React.ReactEventHandler<HTMLMediaElement> = (
    e
  ) => {
    this.setState({
      duration: e.currentTarget.duration,
      position: e.currentTarget.currentTime
    });
  };

  protected _handleError: React.ReactEventHandler<HTMLMediaElement> = () => {
    this._timer.stop();
  };

  protected _setVolume = (value: number) => {
    const mediaEl = this._mediaRef.current;
    if (mediaEl) {
      mediaEl.volume = value;
    }
  };

  protected _muteOrUnMute = () => {
    if (this._mediaRef.current?.muted) {
      this.setState({ muted: false });
      return;
    }
    this.setState({ muted: true });
  };

  protected _handleSliderVolumeChange = (
    ev: React.ChangeEvent<{}>,
    value: number | number[]
  ) => {
    if (Array.isArray(value)) {
      value = value[0];
    }
    this._setVolume(value);
    this.setState({
      volume: value,
      muted: false
    });
  };
}
