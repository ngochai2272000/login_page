export interface IBrowserTab {
  id: string;
  ownerId: string;
  favicons?: string[];
  title?: string;
}

export class BrowserTab implements IBrowserTab {
  id: string;
  ownerId!: string;
  favicons?: string[];
  title?: string;
  constructor() {
    this.id = `tab-${Date.now()}-${Math.floor(Math.random() * 1000)}`;
    this.title = 'New Tab';
  }
}
