import { Button, withStyles } from '@material-ui/core';
import {
  __COLOR_PRIMARY_VAR,
  __COLOR_SECONDARY_VAR
} from '../../constance/style-var';

export default withStyles({
  root: {
    textTransform: 'none'
  },
  containedPrimary: {
    backgroundColor: `var(${__COLOR_PRIMARY_VAR})`,
    '&:hover': {
      backgroundColor: `var(${__COLOR_PRIMARY_VAR}-hover)`
    }
  },
  containedSecondary: {
    backgroundColor: `var(${__COLOR_SECONDARY_VAR})`,
    '&:hover': {
      backgroundColor: `var(${__COLOR_SECONDARY_VAR}-hover)`
    }
  },
  outlinedPrimary: {
    color: `var(${__COLOR_PRIMARY_VAR})`,
    borderColor: `var(${__COLOR_PRIMARY_VAR})`,
    '&:hover': {
      color: `var(${__COLOR_PRIMARY_VAR}-hover)`,
      borderColor: `var(${__COLOR_PRIMARY_VAR}-hover)`
    }
  },
  outlinedSecondary: {
    color: `var(${__COLOR_SECONDARY_VAR})`,
    borderColor: `var(${__COLOR_SECONDARY_VAR})`,
    '&:hover': {
      color: `var(${__COLOR_SECONDARY_VAR}-hover)`,
      borderColor: `var(${__COLOR_SECONDARY_VAR}-hover)`
    }
  },
  textPrimary: {
    color: `var(${__COLOR_PRIMARY_VAR})`
  },
  textSecondary: {
    color: `var(${__COLOR_SECONDARY_VAR})`
  }
})(Button);
