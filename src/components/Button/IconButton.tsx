import { IconButton, PropTypes, withStyles } from '@material-ui/core';
type ColorTypes = PropTypes.Color | 'danger';

export default withStyles({
  root: {
    color: '#FFFFFF',
    '&.Mui-disabled': {
      color: '#FFFFFF',
      background: 'var(--color-disabled)',
    },
  },
  colorInherit: {
    background: 'var(--color-danger)',
    '&:hover': {
      background: 'var(--color-danger-hover)',
    },
  },
  colorPrimary: {
    background: 'var(--color-primary)',
    '&:hover': {
      background: 'var(--color-primary-hover)',
    },
  },
  colorSecondary: {
    background: 'var(--color-secondary)',
    '&:hover': {
      background: 'var(--color-secondary-hover)',
    },
  },
  // tslint:disable-next-line: no-any
})(({ color, ...rest }: { color: ColorTypes; [key: string]: any }) => (
  <IconButton color={color === 'danger' ? 'inherit' : color} {...rest} />
));
