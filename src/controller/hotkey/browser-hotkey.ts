import EventEmitter from 'events';

class BrowserHotkey extends EventEmitter {
  constructor() {
    super();

    this._handleKeyDown = this._handleKeyDown.bind(this);
    window.addEventListener('keydown', this._handleKeyDown);
  }

  public dispose() {
    window.removeEventListener('keydown', this._handleKeyDown);
  }

  _handleKeyDown(ev: KeyboardEvent) {
    if (!ev.key) {
      return;
    }
    const key = ev.key.toLowerCase();
    switch (key) {
      case 'control':
      case 'alt':
      case 'shift':
        return;
      case 'f1':
      case 'f2':
      case 'f3':
      case '0':
      case '1':
      case '2':
      case '3':
      case '4':
      case '5':
      case '6':
      case '7':
      case '8':
      case '9':
        if (ev.ctrlKey || ev.altKey) {
          ev.preventDefault();
        }
        break;
      default:
        break;
    }
    let hotkey = key;
    if (ev.shiftKey) {
      hotkey = 'shift+' + hotkey;
    }
    if (ev.altKey) {
      hotkey = 'alt+' + hotkey;
    }
    if (ev.ctrlKey) {
      hotkey = 'ctrl+' + hotkey;
    }
    console.log(hotkey);
    this.emit('hotkey', ev, hotkey);
  }
}

export default BrowserHotkey;
