import { pckg } from './app-config';
import { IpcChannels, ipcRenderer } from './electron';
import { AIAPermissions, IPermissionPayload } from './permission';
export class SystemCore {
  private static _instance?: SystemCore;
  public static get instance() {
    return SystemCore._instance ?? (SystemCore._instance = new SystemCore());
  }

  public requestPermission(permission: AIAPermissions, payload?: any) {
    const arg: IPermissionPayload = {
      appId: pckg.name,
      permission,
      payload
    };
    console.log('request permission', arg);
    return !!ipcRenderer?.sendSync(IpcChannels.RequestPermission, arg);
  }
}
