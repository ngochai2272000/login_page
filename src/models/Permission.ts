export interface Permission {
  id: string;
  name: string;
  code: string;
}

export interface Permissions {
  data: Permission[];
  count: number;
}
