import { ApolloProvider, ApolloClient, InMemoryCache } from "@apollo/client";
import React from 'react';
import Main from './pages';

const token = localStorage.getItem('token')

const client = new ApolloClient({
  uri: "http://a.vipn.net:4000/",
  cache: new InMemoryCache(),
  headers: {
    authorization: token ? `${token}` : '',
  },

});

class App extends React.Component {

  render() {
    return (
      <ApolloProvider client={client}>
        <Main />
      </ApolloProvider>
    )
  }
}

export default App;
