import { ApolloClient, createHttpLink, InMemoryCache } from '@apollo/client';
import { setContext } from '@apollo/client/link/context';
import LocalStorageHelper from '../../helpers/local-storage';

const httpLink = createHttpLink({
  uri: 'http://a.vipn.net:4000/'
});

const authLink = setContext((_, { headers }) => {
  // get the authentication token from local storage if it exists
  const token = LocalStorageHelper.token;
  const device = LocalStorageHelper.device;
  // return the headers to the context so httpLink can read them
  return {
    headers: {
      ...headers,
      authorization: token ? `Bearer ${token}` : '',
      'device-code': device
    }
  };
});

const apolloClient = new ApolloClient({
  link: authLink.concat(httpLink),
  cache: new InMemoryCache()
});

export default apolloClient;

export function logout() {
  return apolloClient.resetStore();
}
