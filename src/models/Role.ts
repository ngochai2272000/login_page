import { Permission } from './Permission';

export interface Role {
  id: string;
  name: string;
  code: string;
  permissions: Permission[];
}

export interface Roles {
  data: Role[];
  count: number;
}
