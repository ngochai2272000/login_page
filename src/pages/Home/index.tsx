import React from 'react';
import { RouterProps } from 'react-router';
import { Button } from '../../components/Button';
import { Root } from './styled';


type P = RouterProps;

type S = {
  id: number;
};

const initState = Object.freeze<S>({
  id: 123
});

export class HomePage extends React.Component<P, S> {
  state = initState;
  token = localStorage.getItem("token")

  onLogout = () => {
    localStorage.removeItem('token');
    this.props.history.push("/login");
  }

  render() {
    return <Root>
      {!this.token && this.props.history.push("/login")}
      Homepage

      <Button type="button" onClick={this.onLogout}>
        Logout
      </Button>
    </Root>
  }
}
