import { TableBody, TableContainer, TableRow } from '@material-ui/core';
import React from 'react';
import { Table as StyledTable, TCell, THead, TRow } from './styled';

type TableColumnType = {
  key?: React.Key | null;
  dataIndex: string | string[];
  title: string | JSX.Element;
  render?: (data: any, row: any) => string | JSX.Element;
  align?: 'left' | 'right' | 'center';
};

type TableProps = {
  columns: TableColumnType[];
  dataSource?: any[];
  rowKey: (row: any, index: number) => React.Key | null | undefined;
  size?: 'medium' | 'small';
  onRowClick?: (row: any, index: number) => void;
};

function extractDataKey(
  dataIndex: string | string[],
  row: any,
): string | number | undefined {
  if (typeof dataIndex === 'string') {
    return row[dataIndex];
  }
  if (Array.isArray(dataIndex)) {
    const accessKeyRecurse = (obj: any, keys: string[]): any => {
      const [key, ...rest] = keys;
      if (rest.length && obj[key]) {
        return accessKeyRecurse(obj[key], rest);
      }
      return obj[key];
    };
    return accessKeyRecurse(row, dataIndex);
  }
  return row;
}

const Table: React.FC<TableProps> = ({
  columns,
  dataSource,
  rowKey,
  size = 'medium',
  onRowClick,
}) => {
  return (
    <TableContainer>
      <StyledTable>
        <THead>
          <TableRow>
            {columns?.map((col, idx) => (
              <TCell
                className={
                  idx === 0
                    ? 'first-cell'
                    : idx === columns.length - 1
                    ? 'last-cell'
                    : ''
                }
                size={size}
                key={col.key}
                align={col.align}>
                {col.title}
              </TCell>
            ))}
          </TableRow>
        </THead>
        <TableBody>
          {dataSource?.map?.((row, idx) => (
            <TRow
              onClick={() => onRowClick?.(row, idx)}
              key={rowKey?.(row, idx)}>
              {columns?.map((col, cIdx) => {
                const colData = extractDataKey(col.dataIndex, row);
                return (
                  <TCell
                    size={size}
                    className={
                      cIdx === 0
                        ? 'first-cell'
                        : cIdx === columns.length - 1
                        ? 'last-cell'
                        : ''
                    }
                    key={col.key}
                    align={col.align}>
                    {col.render?.(colData, row) || colData?.toString()}
                  </TCell>
                );
              })}
            </TRow>
          ))}
        </TableBody>
      </StyledTable>
    </TableContainer>
  );
};

export default Table;
