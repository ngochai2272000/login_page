import Enum from './enum';

const electron: typeof Electron | undefined = window.require?.('electron');
const ipcRenderer: Electron.IpcRenderer | undefined = electron?.ipcRenderer;

export default electron;
export { ipcRenderer };

export const IpcChannels = Object.freeze({
  RequestPermission: 'aia/REQUEST_PERMISSION' as 'aia/REQUEST_PERMISSION'
});
export type IpcChannels = Enum<typeof IpcChannels>;
