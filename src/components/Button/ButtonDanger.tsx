import { Button, withStyles } from '@material-ui/core';
import { __COLOR_DANGER_VAR } from '../../constance/style-var';

export default withStyles({
  root: {},
  containedPrimary: {
    backgroundColor: `var(${__COLOR_DANGER_VAR})`,
    '&:hover': {
      backgroundColor: `var(${__COLOR_DANGER_VAR}-hover)`,
    },
  },
  outlinedPrimary: {
    color: `var(${__COLOR_DANGER_VAR})`,
    borderColor: `var(${__COLOR_DANGER_VAR})`,
    '&:hover': {
      color: `var(${__COLOR_DANGER_VAR}-hover)`,
      borderColor: `var(${__COLOR_DANGER_VAR}-hover)`,
    },
  },
  textPrimary: {
    color: `var(${__COLOR_DANGER_VAR})`,
    '&:hover': {
      color: `var(${__COLOR_DANGER_VAR}-hover)`,
    },
  },
})((props) => <Button {...props} color="primary" />);
