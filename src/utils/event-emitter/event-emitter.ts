import { EmitEventListener, IEventEmitter } from './type';

export class EventEmitter implements IEventEmitter {
  protected _events: { [event: string]: EmitEventListener[] } = {};

  public hasListener(event: string) {
    return !!this._events[event]?.length;
  }

  public on(event: string, listener: EmitEventListener): EventEmitter {
    if (!Array.isArray(this._events[event])) {
      this._events[event] = [];
    }

    if (typeof listener !== 'function') {
      throw new Error(`listener must be a function`);
    }

    this._events[event].push(listener);
    return this;
  }

  public removeListener(event: string, listener: EmitEventListener): void {
    if (!Array.isArray(this._events[event])) {
      return;
    }

    this._events[event] = this._events[event].filter(
      (item) => item !== listener,
    );
  }

  public removeAllListeners(): void {
    this._events = {};
  }

  public emit(event: string, ...args: any[]): void {
    if (typeof this._events[event] !== 'object') {
      return;
    }

    [...this._events[event]].forEach((listener) => listener.apply(this, args));
  }
}
